<?php
/**
 * index.php
 *
 * set paths for including
 * include the require bootstrap and autoload
 */
namespace Minimal;

/**
 * @todo in configuration
 */
ini_set('display_errors', 'on');
error_reporting(E_ALL);

// define the base paths
define('APPLICATION_PATH', realpath($_SERVER['DOCUMENT_ROOT'] . '/../application'));
define('BASE_PATH', realpath($_SERVER['DOCUMENT_ROOT'] . '/../' ));

// update the include paths
set_include_path(get_include_path() . PATH_SEPARATOR . APPLICATION_PATH);
set_include_path(get_include_path() . PATH_SEPARATOR . BASE_PATH . '/vendor/');

// include the autoloader
require "Minimal/Autoload.php";

// start it up
require "Minimal/Bootstrap.php";
