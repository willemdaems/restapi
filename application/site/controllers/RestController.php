<?php
/**
 * RestController.php
 *
 * Communication between client and smartblock
 * Creates a new REST application
 * The REST Application takes one controller and executes
 * 4 commands in total: PUT, GET, UPDATE, DELETE
 *
 * These commands are executed with the data gotten from the AJAX request.
 *
 * @example
 * <code>
 * $json = json_encode(array("user" => "johndoe"));
 * $ch = curl_init();
 * $options = array(
 *         CURLOPT_RETURNTRANSFER => true,
 *         CURLOPT_CUSTOMREQUEST => 'GET',
 *         CURLOPT_HTTPHEADER => array(
 *             'Content-Type: application/json',
 *             'Content-Length: ' . strlen($json),
 *             'data:' . $json
 *         ),
 *         CURLOPT_URL => "http://rtvnh/restapp"
 * );
 *
 * curl_setopt_array($ch, $options);
 *
 * $result = curl_exec($ch);
 *
 * curl_close($ch);
 * </code>
 *
 * @package RestApplication
 * @subpackage REST
 * @version 1.0.0
 * @author WillemDaems
 */

namespace site\controllers;

class RestController
{
    /**
     * The RESTfull Application
     * @var RtvSocial_Library_Rest_Application
     */
    private $application;

    // you dummy!
    public function init()
    {
        $this->processrequestAction();
    }

    /**
     * Analyses the request header.
     * 1. valid application/json content-type
     * 2. GET, PUT, UPDATE, DELETE methods only
     *
     * if those not validate the application will immediately quit and
     *  output the headers with an error message as json
     *
     * After validation it returns data as json from the response object
     *
     * @return void
     */
    public function processrequestAction()
    {
        // fetch the headers
        $headers = getallheaders();

        // extract the content type, this is for validating the correct json/application
        $contentType = $this->fetchContenttype($headers);

        // get PUT,UPDATE,DELETE etc. The custom -X header
        $request = $this->getRequesttype();

        // new default application
        $this->application = new \Rest\Application\Application($headers, $contentType, $request);

        // which section do we want action on
        $section = $this->getParam(2);

        if (!empty($section)) {

            // prepare the name
            $section = ucfirst($section);

            $controllerName = "\\Rest\\Endpoints\\" . $section;

            try {
                $controller = new $controllerName;
                $this->application->setController($controller);
            } catch (Exception $e) {
                // log the error
                $this->application->getResponse()->errorMessage = 'Unknown error occurred';
                $this->application->getResponse()->error(500);
            }
        }

        // execute the request through a command
        $this->application->execute();
        exit;
    }

    /**
     * Extract content-type header from the given header array
     * @param array $headers
     * @return string | bool
     */
    private function fetchContenttype(array $headers)
    {
        $contentType = false;

        foreach ($headers as $key => $value) {

            $key = strtolower($key);

            if ($key === 'content-type') {
                $contentType = strtolower($value);
                break;
            }

        }
        return $contentType;
    }

    /**
     * Extract user request type
     * @return string | bool
     */
    private function getRequesttype()
    {
        $request = false;

        if (!empty($_SERVER['REQUEST_METHOD'])) {
            $request = strtoupper($_SERVER['REQUEST_METHOD']);
            $request = trim($request);
        }

        return $request;
    }

    /**
     * Gets a param from the param stack
     *
     * @param string|int $index
     * @return string
     */
    public function getParam($index)
    {
        $value = '';

        $parts = explode("/", $_SERVER['REQUEST_URI']);

        if (!empty($parts[$index])) {
            $value = $parts[$index];
        }

        return $value;
    }
}
