<?php
/**
 * RestAbstract.php
 *
 * Abstract class with the ONLY four allowed methods for the RESTful API
 */
namespace Rest\Application;

abstract class RestAbstract
{
    abstract public function put($json, \Rest\Response $responseObject);
    abstract public function get($json, \Rest\Response $responseObject);
    abstract public function update($json, \Rest\Response $responseObject);
    abstract public function delete($json, \Rest\Response $responseObject);
}