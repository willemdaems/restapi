<?php

class Rtvsocial_Library_Rest_ErrorMessages
{

    // USER ERRORS
    public static $USER_EMAIL_INVALID = array('errorCode' => 400110, 'errorMessage' => 'Invalid email address');
    public static $USER_EMAIL_EXISTS = array('errorCode' => 400120, 'errorMessage' => 'Email address exists');
    public static $USER_USERNAME_INVALID = array('errorCode' => 400210, 'errorMessage' => 'Invalid username');
    public static $USER_USERNAME_EXISTS = array('errorCode' => 400220, 'errorMessage' => 'Username exists');
}
