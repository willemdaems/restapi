<?php
/**
 * Application.php
 *
 * REST base application
 * Based on strategy pattern
 * Holds response object which always returns header status
 *
 * @version 1.0.0
 * @author WillemDaems
 * @package Rtvsocial
 * @subpackage RestApplication
 *
 */
namespace Rest\Application;


class Application implements \Rest\Application\IRestApplication
{
    private $host = 'localhost';

    private $database = 'rest';

    private $username = 'root';

    private $password = 'root';

    /**
     * The object that holds the commands being invoked
     * @var RtvSocial_Library_Rest_Abstract
     */
    private $controller;

    /**
     * The response object for outputting headers and data
     * @var Rtvsocial_Library_Rest_Response
     */
    private $response;

    /**
     * Headers from the ajax/curl request
     * @var array
     */
    private $headers;

    /**
     * The content-type that came with the headers
     * @var string
     */
    private $contentType;

    /**
     * The request method asked
     * @var string
     */
    private $request;

    /**
     * JSON decoded content. Given by header "data"
     * @var type
     */
    private $json;

    /**
     * Database
     * @var Zend_Database_Adapter
     */
    private $db;

    /**
     * Methods a user can invoke
     * @var array
     */
    private $methods = array('GET', 'PUT', 'UPDATE', 'DELETE', 'OPTIONS');

    /**
     * Construct new response object and assign variables
     * @param array $headers
     * @param string $contentType
     * @param string $request
     * @return void
     */
    public function __construct($headers, $contentType, $request)
    {
        // new response object
        $this->response = new \Rest\Response;

        // assign array $headers
        $this->headers = $headers;

        // assign content-type
        $this->contentType = $contentType;

        // assign the custom request header
        $this->request = $request;

        // assign db adapter
        $this->db = $this->getDatabase();
    }

    /**
     * Create a new object to interact
     * @param RtvSocial_Library_Rest_Abstract $controller
     * @return void
     */
    public function setController(\Rest\Application\RestAbstract $controller)
    {
        // assign controller
        $this->controller = $controller;

        // inject db
        // $this->controller->setDatabase($this->db);
    }

    /**
     * Getter for the response object
     * @return Rtvsocial_Library_Rest_Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Executes the command requested by the user
     * @return void
     */
    public function execute()
    {
        // validate if everything is ok to continue, bad requests will stop the application after output
        $this->validateHeaders();

        // validate the apikey, every error immediately quits the application
        $this->validateKey();

        // just tolowercase for being neat
        $method = strtolower($this->request);

        // for return object
        $this->response->method = $method;

        // only to prevent illegal invocation and big fat errors that come with them
        if (method_exists($this->controller, $method)) {

            // invoke the method, it will return itself
            $this->controller->$method($this->json, $this->response);
        }

        $this->response->toJson();
    }

    /**
     * Validate all headers and create an error if the headers are illegal
     * ALWAYS show what is accepted if headers are incorrect
     * @return boolean
     */
    private function validateHeaders()
    {
        if ($this->contentType !== 'application/json') {
            $this->response->status = 400;
            $this->response->errorMessage = 'content-type "' . $this->contentType .'" is not allowed';
            $this->response->error(400, 'application/json');
            return false;
        }

        // not really an error but still dont continue the app
        if (empty($this->request)) {
            $this->response->options();
            return false;
        }

        // this is actually a catch for internet explorer
        // it can't handle custom request methods
        $allHeaders = getallheaders();

        if (!empty($allHeaders['type'])) {
            $this->request = $allHeaders['type'];
        }

        // method is not allowed here
        if (in_array($this->request, $this->methods) === false) {
            $this->response->status = 405;
            $this->response->errorMessage = 'Method: "' . $this->request .'" is not allowed';
            $this->response->error(405, implode(", ", $this->methods));
            return false;
        }

        // check for malformed json data
        if (!empty($this->headers['data'])) {

            $this->json = json_decode($this->headers['data'], false);

            // json error, generate error and quit
            if ($this->json === null) {

                $this->response->status = 409;
                $this->response->errorMessage = 'malformed JSON';
                $this->response->error(409, 'JSON Format:
                    {"data": {
                        "property": "value",
                        "property": 2
                    }');
            }

            return false;
        }
    }

    /**
     * Check for APIKEY provided by rtvnh
     * @return void
     */
    public function validateKey()
    {
        // absolutely nothing came with the request
        if (empty($this->json)) {
            $this->response->status = 401;
            $this->response->errorMessage = "No valid apikey found";
            $this->response->error(401, '{"apikey": "xxxxxxxxxxxxxxxx"}');
        }

        // no key is given
        if (!property_exists($this->json, 'apikey')) {

            $this->response->status = 401;
            $this->response->errorMessage = "No valid apikey found";
            $this->response->error(401, '{"apikey": "xxxxxxxxxxxxxxxx"}');
        }

        $stmt = $this->db->prepare("
                SELECT
                    allowed_domains
                FROM
                    rest_apikey
                WHERE
                    `key` = :key AND expires > '".date("Y-m-d H:i:s", time())."'
            ");

        $stmt->execute(array(':key' => $this->json->apikey));

        try {
            $row = $stmt->fetchObject();
        } catch (Exception $e) {
            // log the error
            // output a 500 header status error
            $this->error(500);
        }

        // apikey is expired or just not found
        if (empty($row)) {
            $this->response->status = 401;
            $this->response->errorMessage = "No valid apikey found";
            $this->response->error(401, '{"apikey": "xxxxxxxxxxxxxxxx"}');
        }

        $json = json_decode($row->allowed_domains);

        if (in_array($this->headers['Host'], $json->data) === false) {
            $this->response->status = 401;
            $this->response->errorMessage = "Host not allowed";
            $this->response->error(401, '{"apikey": "xxxxxxxxxxxxxxxx"}');
        }
    }

    private function getDatabase()
    {
        try {
            $conn = new \PDO(
                "mysql:dbname=".$this->database.";host=" . $this->host,
                $this->username,
                $this->password,
                array(
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_TIMEOUT => 4
                )
            );
        } catch (\PDOException $e) {
            echo $e->getMessage();
            exit;
        }

        return $conn;
    }
}
