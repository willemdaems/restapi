<?php
/**
 * IRestApplication.php
 *
 * Interface for new Rest endnode application
 *
 * @package Dash
 * @subpackage Rest
 */
namespace Rest\Application;

interface IRestApplication
{
    public function setController(\Rest\Application\RestAbstract $controller);
    public function execute();
}