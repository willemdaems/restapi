<?php
/**
 * Auth.php
 * Command pattern
 * End node
 *
 * curl -XGET --header "Content-Type: application/json" --header 'data:{"apikey":"yourkey","userid":2}'  http://rtvnh/restapp/User -v

 */

namespace Rest\Endpoints;

class User extends \Rest\Application\RestAbstract
{
    /**
     * Inject db
     * @param type $db
     * @return void
     */
    public function setDatabase($db)
    {

    }

    public function get($json, \Rest\Response $responseObject)
    {
        // change the http status code
        $responseObject->status = 200;

        // assign a variable
        $responseObject->called = 'get';

        // assign another variable
        $responseObject->message = 'KTHNBYE';
    }

    public function put($json, \Rest\Response $responseObject)
    {
        // do some magic here
        // store something in the database perhaps

        // put anything in the responseObject, it will be json_encoded
        // when it is send back
        $responseObject->status = 201;
        $responseObject->called = 'get';

        // $json would be the object gotten from
        // our javascript / curl call
        $responseObject->myKey = $json->apikey;
    }

    public function update($json, \Rest\Response $responseObject) {}
    public function delete($json, \Rest\Response $responseObject) {}
}
