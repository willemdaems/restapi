<?php
/**
 * Response.php
 * REST response object, default.
 *
 * Return a json representation of this object depending on what the application
 * inject through the magic get/set methods
 *
 * @package Dash
 * @subpackage REST
 * @version 1.0
 * @author WillemDaems
 */

namespace Rest;

class Response
{

    protected $status = 200;

    private $accept = '';

    /**
     * The variable collection which will be
     * presented as json
     * @var type
     */
    private $variables = array();

    /**
     * Set a variable in the collection
     * @param string $name
     * @param string | int | array $value
     */
    public function __set($name, $value)
    {
        $this->variables[$name] = $value;
    }

    /**
     * Return a variable from the collection
     * @param string $name
     * @return string | int | array
     */
    public function __get($name)
    {
        if (isset($this->variables[$name])) {
            return $this->variables[$name];
        } else {
            return '';
        }
    }

    /**
     * Check if variable exists in the collection
     * @param string $name
     * @return boolean
     */
    public function __isset($name)
    {
        if (isset($this->variables[$name])) {
            return true;
        } else {
            return false;
        }
    }

    public function error($status, $accept = '')
    {
        $this->status = $status;
        $this->accept = $accept;

        echo $this->toJson();
        exit;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * JSON Encode the variable collection
     * Set proper headers for ajax calls
     *
     * @return string json
     */
    public function toJson()
    {
        $json = json_encode($this->variables);
        $status = 200;

        if (!empty($this->variables['status'])) {
            $status = $this->variables['status'];
        }

        header("HTTP/1.1 " . $status);
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: data, Origin, X-Requested-With, Content-Type, Accept");
        header("Content-Length:" . strlen($json));
        header("Content-Type: application/json");

        if (!empty($this->accept)) {
            header("Accept: " . $this->accept);
        }

        echo $json;
        //flush();
    }
}