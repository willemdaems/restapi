<?php
/**
 * View.php
 *
 * View object for rendering pages or small blocks
 *
 * @package Dash
 * @subpackage View
 * @version 1.0
 */
namespace Minimal;

class View
{
    /**
     * Holds current view directoy location
     * @var string
     */
    private $directoryLocation;

    public function __construct() {}

    /**
     * Default path is injection from the bootstrap
     * @param string $path
     * @return void
     */
    public function setDirectoryLocation($path)
    {
        $this->directoryLocation = $path;
    }

    /**
     * Default path is injection from the bootstrap
     * @param string $path
     * @return void
     */
    public function getDirectoryLocation()
    {
        return $this->directoryLocation;
    }

    /**
     * Renders a view file
     * @param string $file
     * @return string
     */
    public function render($file = '')
    {
        if (empty($file)) {
            return $this->content;
        }

        // test if there is a valid extension
        if (preg_match("/\.phtml$/i", $file) === 0) {
            $file .= '.phtml';
        }

        $path = strtolower($this->getDirectoryLocation()) . strtolower($file);

        ob_start();
        require $path;
        $this->content = ob_get_contents();
        ob_end_clean();

        return $this->content;
    }

    /**
     * Magic get method
     *
     * @todo checks valid names
     * @param string $name
     * @return string
     */
    public function __get($name)
    {
        return $name;
    }

    /**
     * Magic setter
     * @param string $name
     * @param string $value
     * @return void
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * Validates if a path really exists
     *
     * @param string $path
     * @return boolean
     */
    private function isValidPath($path)
    {

        $valid = false;
        if (is_readable(APPLICATION_PATH . '/' . $path)) {
            $valid = true;
        }

        if (is_readable($path)) {
            $valid = true;
        }

        return $valid;
    }
}