<?php
/**
 * Front.php
 *
 * Frontend bootstrap
 * @package Dash
 * @subpackage Bootstrap
 * @version 1.0
 */
namespace Minimal;

class Bootstrap
{

    public static function run()
    {
        // start the autoloader
        Autoload::init();

        $routing = self::getRouting();

        // create a namespace path where it can be found
        $controllerName = '\\site\controllers\\' . $routing['controller'] . 'Controller';

        // get the action method from the routing object
        $action = $routing['action'] . 'Action';

        // load a new controller
        $controller = new $controllerName;
        $controller->view = new \Minimal\View;
        $controller->view->setDirectoryLocation( 'site/views/html/'. $routing['controller'] . '/');

        // perform the action corresponding to request
        if (method_exists($controller, 'init')) {
            $controller->init();
        }

        // perform the action corresponding to request
        if (method_exists($controller, $action)) {
            $controller->$action();
        }

        echo $controller->view->render($routing['action']);
    }

    public static function getRouting()
    {
        $uri = preg_replace("/[^A-Za-z0-9\/]/", "", $_SERVER['REQUEST_URI']);

        $parts = explode("/", $uri);

        if (empty($parts[1])) {
            $controllerName = 'Index';
        } else {
            $controllerName = ucfirst($parts[1]);
        }

        if (empty($parts[2])) {
            $actionName = 'Index';
        } else {
            $actionName = ucfirst($parts[2]);
        }

        return array('controller' => $controllerName, 'action' => $actionName);
    }
}

Bootstrap::run();