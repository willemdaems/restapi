
RESTful API

Included is a mini framework just to get things going and see the working
example from a jQuery request with a custom header and application/json header.

One can build as many applications or endnodes as wanted.

The APIKey will be validated per domain which are stored in the rest_key table

Some examples:

cURL:

    The data variable will be set in the Endpoint object and data is ready to use
    curl -XGET --header "Content-Type: application/json" --header 'data:{"apikey":"yourKey","someInt":2}'  http://yourdomain.com/rest/User -v


PHP cURL:

    $json = json_encode(array("user" => "willem"));
    $ch = curl_init();
    $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json),
                'data:' . $json
            ),
            CURLOPT_URL => "http://yourdomain.com/rest/User"
    );

    curl_setopt_array($ch, $options);

    $result = curl_exec($ch);
    curl_close($ch);
